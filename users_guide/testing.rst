Testing
=======

.. module:: ferris.tests.lib

Ferris provides utilities to test your application using `nose <https://nose.readthedocs.org/en/latest/>`_ and `webtest <http://webtest.pythonpaste.org/en/latest/>`_.


Installing the nose plugin
--------------------------

`FerrisNose <https://pypi.python.org/pypi/FerrisNose>`_ is a nose plugin that allows you to run isolated Google App Engine tests. Install it via pip::

    pip install ferrisnose


.. note:: You may have to run this with sudo on Linux and OS X.

.. note:: You may need to specify the --pre flag to pip.


Running tests
-------------

To run tests just use nose::

    nosetests --with-ferris app/tests

.. warning:: be sure to specify a path or nose will try to discover all tests. We only need the test cases in /app/tests.


Writing tests for models
------------------------

Models and other parts of the application that don't involve HTTP/WSGI (such as services) can be tested using :class:`AppEngineTest`.

.. autoclass:: AppEngineTest

Here is a trivial example::

    from app.models.cat import Cat
    from ferrisnose import AppEngineTest

    class CatTest(AppEngineTest):
        def test_herding(self):
            Cat(name="Pickles").put()
            Cat(name="Mr. Sparkles").put()

            assert Cat.query().count() == 2


Writing tests for controllers
-----------------------------

Controller, components, and templates usually have to be tested within a full WSGI environment. This is where :class:`AppEngineWebTest` comes in handy:

.. autoclass:: AppEngineWebTest

To add controllers to the testapp use :meth:`add_controller`.

.. automethod:: AppEngineWebTest.add_controller

Here's an example of writing a test case for a single controller::

    from app.controllers.cats import Cats
    from ferrisnose import AppEngineWebTest

    class CatsTest(AppEngineWebTest):
        def test_herding_method(self):
            self.add_controller(Cats)

            r = self.testapp.get('/cats')

            assert "Pickles" in r


Writing tests for the entire application
----------------------------------------
    
Sometimes you want the entire application to be up and running. In this instance you can use :class:`FerrisAppTest`. Like :class:`AppEngineWebTest` it exposes a webtest instance via ``self.testapp`` but it automatically includes all controllers and plugins defined in your app. 

.. autoclass:: FerrisAppTest

This allows you to make requests to your application just as you would if the application were running::

    from ferrisnose import FerrisAppTest

    class CatsTest(FerrisAppTest):
        def test_herding(self):
            Cat(name="Pickles").put()
            Cat(name="Mr. Sparkles").put()

            r = self.testapp.get('/cats')

            assert "Pickles" in r
            assert "Mr. Sparkles" in r
